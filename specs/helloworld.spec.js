describe('Hello World', function () {
  it('should run this smoke test', function () {
    expect(true).toBeTruthy();
  });

  it('returns Hello World', function () {
    expect(helloWorld()).toBe('HelloWorld');
  });
});
